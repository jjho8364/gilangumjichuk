package com.gilang.umjichuktv.activity.list;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gilang.umjichuktv.R;
import com.gilang.umjichuktv.adapter.GridDramaAdapter;
import com.gilang.umjichuktv.item.GridDramaItem;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class BujaEpsListActivity extends Activity {
    private String TAG = " BujaEpsListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;

    private ArrayList<GridDramaItem> listViewItemArr;
    private GridView gridView;
    private ListView btnListView;
    private LinearLayout youtubeAd;

    private int adsCnt = 0;
    private String listUrl = "";

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=800;
    private long mLastClickTime;

    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    private String nextUrl = "";
    private String nextTitle = "";
    private String nextImgUrl = "";

    GetIframe getIframe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_buja_list);

        /*AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);*/

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");

        pref= BujaEpsListActivity.this.getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

        tv_title = (TextView)findViewById(R.id.tv_title);
        img_poster  = (ImageView)findViewById(R.id.img_poster);
        tv_title.setText(title);

        /*youtubeAd = (LinearLayout)findViewById(R.id.youtubead);
        youtubeAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "clicked youtube");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCDWC_3sE9EuVHTDI-OZEXgw"));
                startActivity(intent);
            }
        });*/

        if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }

        gridView = (GridView)findViewById(R.id.gridview);
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getGridView = new GetGridView();
        getGridView.execute();

    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        ArrayList<String> btnTextArr = new ArrayList<String>();
        ArrayList<String> btnVideoUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridDramaItem>();

            mProgressDialog = new ProgressDialog(BujaEpsListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                Elements elements = doc.select("#load_video.entry_list .each-video");

                for(Element element: elements) {

                    String title = element.select(".item-thumbnail a").attr("title");
                    String imgUrl = element.select(".item-thumbnail img").attr("src");
                    String listUrl = element.select(".item-thumbnail a").attr("href");
                    String update = element.select("p").text();

                    GridDramaItem gridViewItemList = new GridDramaItem(title, update, imgUrl, listUrl);
                    listViewItemArr.add(gridViewItemList);
                }


            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(BujaEpsListActivity.this != null && listViewItemArr.size() != 0){
                gridView.setAdapter(new GridDramaAdapter(BujaEpsListActivity.this, listViewItemArr, R.layout.item_grid_drama));

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        long currentClickTime= SystemClock.uptimeMillis();
                        long elapsedTime=currentClickTime-mLastClickTime;
                        mLastClickTime=currentClickTime;

                        // 중복 클릭인 경우
                        if(elapsedTime<=MIN_CLICK_INTERVAL){
                            return;
                        }

                        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.

                            nextUrl = listViewItemArr.get(position).getListUrl();
                            nextTitle = listViewItemArr.get(position).getTitle();
                            nextImgUrl = listViewItemArr.get(position).getImgUrl();

                            interstitialAd.show();

                        } else {
                            nextUrl = listViewItemArr.get(position).getListUrl();
                            nextTitle = listViewItemArr.get(position).getTitle();
                            nextImgUrl = listViewItemArr.get(position).getImgUrl();

                            getIframe = new GetIframe();
                            getIframe.execute();

                            /*Intent intent = new Intent(BujaEpsListActivity.this, MaruEpsListActivity.class);
                            intent.putExtra("listUrl", intentListUrl);
                            intent.putExtra("title", intentTitle);
                            intent.putExtra("imgUrl", intentImgUrl);
                            intent.putExtra("adsCnt", "0");
                            startActivity(intent);*/
                        }
                    }
                });
            }

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    public class GetIframe extends AsyncTask<Void, Void, Void> {

        String iframeUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(BujaEpsListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {
                Log.d(TAG, "nextUrl : " + nextUrl);
                doc = Jsoup.connect(nextUrl).timeout(20000).get();

                iframeUrl = doc.select(".iframetrack iframe").attr("src");

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(BujaEpsListActivity.this != null){
                Intent intent = new Intent(BujaEpsListActivity.this, MaruListActivity2.class);
                intent.putExtra("listUrl", iframeUrl);
                intent.putExtra("title", nextTitle);
                intent.putExtra("imgUrl", nextImgUrl);
                intent.putExtra("adsCnt", "0");
                startActivity(intent);
            }

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getGridView != null){
            getGridView.cancel(true);
        }
        if(getIframe != null){
            getIframe.cancel(true);
        }
    }

}
